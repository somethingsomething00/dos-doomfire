# Doomfire DOS

The doomfire effect for DOS.

![thumbnail](./thumbnail.png)

```
A rewrite of Doomfire for DOS 320x200 VGA mode.

This is the fire effect seen in the PS1 port of DOOM, as well as in DOOM64.

I first stumbled upon the algorithm for this effect on Fabien Sanglard's page (link below).
Take a look at SpreadFire() and DoFire() to get an idea of how it works.
I won't pretend to understand the reasoning as to why this algorithm works, but it produces some pretty neat results.

The random number generator was optimized to a simple lookup table to avoid any unecessary math operations in the main loop.


Build and run:
build.bat
df.exe

Originally compiled with Borland C++ 3.1
Originally tested on dosbox 0.74-3 at ~60,000 cycles (core=dynamic, cpu=auto)

Controls:
V........Ignite Fire
C........Douse Fire
ESC/q....Quit


DOS VGA palette reference:
Black Art of 3D Game Programming
Andre LaMothe

Fabien Sanglard's page on the effect:
https://fabiensanglard.net/doom_fire_psx/

Doomfire:
https://gitlab.com/somethingsomething00/doomfire/

dos-mandelbrot:
https://gitlab.com/somethingsomething00/dos-mandelbrot/
```
